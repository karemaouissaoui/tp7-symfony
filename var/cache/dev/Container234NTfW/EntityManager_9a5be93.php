<?php

namespace Container234NTfW;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder64d35 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer11927 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties40944 = [
        
    ];

    public function getConnection()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getConnection', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getMetadataFactory', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getExpressionBuilder', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'beginTransaction', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getCache', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getCache();
    }

    public function transactional($func)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'transactional', array('func' => $func), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->transactional($func);
    }

    public function commit()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'commit', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->commit();
    }

    public function rollback()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'rollback', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getClassMetadata', array('className' => $className), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'createQuery', array('dql' => $dql), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'createNamedQuery', array('name' => $name), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'createQueryBuilder', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'flush', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'clear', array('entityName' => $entityName), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->clear($entityName);
    }

    public function close()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'close', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->close();
    }

    public function persist($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'persist', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'remove', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'refresh', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'detach', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'merge', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getRepository', array('entityName' => $entityName), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'contains', array('entity' => $entity), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getEventManager', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getConfiguration', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'isOpen', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getUnitOfWork', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getProxyFactory', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'initializeObject', array('obj' => $obj), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'getFilters', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'isFiltersStateClean', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'hasFilters', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return $this->valueHolder64d35->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer11927 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder64d35) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder64d35 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder64d35->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__get', ['name' => $name], $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        if (isset(self::$publicProperties40944[$name])) {
            return $this->valueHolder64d35->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder64d35;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder64d35;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder64d35;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder64d35;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__isset', array('name' => $name), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder64d35;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder64d35;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__unset', array('name' => $name), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder64d35;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder64d35;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__clone', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        $this->valueHolder64d35 = clone $this->valueHolder64d35;
    }

    public function __sleep()
    {
        $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, '__sleep', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;

        return array('valueHolder64d35');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer11927 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer11927;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer11927 && ($this->initializer11927->__invoke($valueHolder64d35, $this, 'initializeProxy', array(), $this->initializer11927) || 1) && $this->valueHolder64d35 = $valueHolder64d35;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder64d35;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder64d35;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
